<?php
namespace Matogen\Processor;

use Matogen\Processor\Matodocs;
use PhpOffice\PhpWord\TemplateProcessor;
use Convertio\Convertio;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\SimpleType\TblWidth;

/**
 * Class derives from class Matodocs. Used for inserting text and image edits in provided docx templates.
 *
 * Docx templates can additionally be converted to PDF.
 */

class DocxProcessor extends Matodocs {

    /**
     * Convertio API
     * @var string
     */
    public $strConvertioAPI;

    /**
     * File after processing
     * @var string
     */
    public $strProcessedFile = '';

    /**
     * Template processor
     * @var TemplateProcessor
     */
    public $classTemplateProcessor;

    function __construct($strConvertioAPI, $strTemplatePath)
    {
        // Set Convertio API
        $this->strConvertioAPI = $strConvertioAPI;

        // Run parent class constructor specifying docx tag.
        parent::__construct($strTemplatePath);

        // Load template into template processor
        $this->classTemplateProcessor = new TemplateProcessor($this->strTemplatePath);
    }

    /**
	* Inserts text, image and table edits in instantiated docx template and returns the processed file.
    *
	* @param array{text_edit: string} $arrText
	* @param array{image_path: string} $arrImages 
    * @param array{table_data: string} $arrTables
    * @param array{table_options: string} $arrTableOptions 
    *
    * @return string Path to temporarily stored processed file.
	*/
    public function processAndConvertFile(
        $arrText=[], 
        $arrImages=[], 
        $arrTables=[],
        $arrTableOptions=['borderSize' => 10, 'borderColor' => 'black', 'width' => 100 * 50]) : string
    {

        // Process text edits
        if (!empty($arrText)) 
        {
            $this->classTemplateProcessor->setValues($arrText);
        }

        // Process table edits
        if (!empty($arrTables))
        {
            foreach ($arrTables as $strTable=>$arrTableData)
            {
                // Create table with given options
                $options = array_merge($arrTableOptions, array('unit' => TblWidth::PERCENT));
                $table = new Table($options);     

                // Create table
                foreach ($arrTableData as $row) 
                {
                    $table->addRow();
                    foreach ($row as $cell) 
                    {
                        if (is_array($cell))
                        {
                            $table->addCell(null, $cell['style'])->addText($cell['value']);
                        } else
                        {
                            $table->addCell()->addText($cell);
                        }
                    }
                }       
                $this->classTemplateProcessor->setComplexBlock($strTable, $table);
            }
        }

        // Process image edits
        if (!empty($arrImages)) 
        {
            // Test the path of each image before inserting into docx template
            foreach ($arrImages as $strKey=>$strImagePath){
                try
                {
                    if (is_string($strImagePath))
                    {
                        if (!realpath($strImagePath)) {
                            throw new Exception('Directory does not exist: ' . $strKey);
                        }
                    } else 
                    {
                        if (!realpath($strImagePath['path'])) {
                            throw new Exception('Directory does not exist ' . $strKey['path']);
                        }
                    }
                }
                catch (Exception $e) 
                {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    exit();
                }
                $this->classTemplateProcessor->setImageValue($strKey, $strImagePath);
            }
        }

        // Store processed file path in class variable 
        // This is to unlink the file when destroying the classTemplateProcessor class
        $this->strProcessedFile = $this->classTemplateProcessor->save();

        // Return path to processed file
        return  $this->strProcessedFile;
    }

    // Save docx file, optionally as a PDF
    public function saveFile($strOutputDir, $strProcessedFile="", $boolSaveAsPDF=true): string
    {
        //Check if directory exist
        $this->makeDirectoryCheck($strOutputDir);

        // Use parent processed file if no other is given
        if (empty($strProcessedFile))
        {
            try
            {
                if (!file_exists($this->strProcessedFile))
                {
                    throw new Exception('No processed file to choose from.');
                }
                $strProcessedFile = $this->strProcessedFile;
            } catch (Exception $e)
            {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                exit();
            }
        }

        // Convert and save template as PDF if specified, otherwise save as docx file
        $strSaveFileName = '';
        if ($boolSaveAsPDF)
        {   
            // Path to file
            $strSaveFileName = $strOutputDir . DIRECTORY_SEPARATOR . 'processed_' . pathinfo($this->strTemplatePath, PATHINFO_FILENAME) . '.pdf';

            // Directly convert processed docx template to PDF
            $this->convertToPDF($strSaveFileName, $strProcessedFile);  
        } else
        {
            // Path to file
            $strSaveFileName = $strOutputDir . DIRECTORY_SEPARATOR . 'processed_' . pathinfo($this->strTemplatePath, PATHINFO_BASENAME);

            // Save processed docx template
            copy($strProcessedFile, $strSaveFileName);
        }
        
        return $strSaveFileName;
    }

    // Convert the instantiated (if no other is provided) document to PDF and save at the instantiated path
    public function convertToPDF($strOutputPath, $strInputPath=''): void
    {
        $API = new Convertio($this->strConvertioAPI); 
        if (empty($strInputPath)){
            //Check if directory exist
            $this->makeDirectoryCheck(pathinfo($strInputPath, PATHINFO_DIRNAME));

            $API->start($this->strTemplatePath, 'pdf')->wait()->download($strOutputPath)->delete();
        } else {
            $API->start($strInputPath, 'pdf')->wait()->download($strOutputPath)->delete();
        }
    }
}
?>