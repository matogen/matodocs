<?php
namespace Matogen\Processor;

use Matogen\Processor\Matodocs;
use Spatie\Browsershot\Browsershot;
use Mustache_Loader_FilesystemLoader;
use Mustache_Engine;

/**
 * Class derives from class Matodocs. Used for inserting text and image edits in provided html templates.
 *
 */
class HTMLProcessor extends Matodocs {

    function __construct($strTemplatePath)
    {
        // Run parent class constructor specifying html tag
        parent::__construct($strTemplatePath);
    }

    /**
	* Inserts text and image edits in instantiated html template.
    *
    * @param array{edits: string} $arrEdits
	* @param boolean $boolSaveAsPDF 
    *
    * @return string Processed HTML text.
	*/
    public function processAndConvertFile($arrEdits=[]) : string
    {

        // Include this when initialising to have mustache search for files with the specified extension
        $options =  array('extension' => '.' . pathinfo($this->strTemplatePath, PATHINFO_EXTENSION));
        
        // Initialise mustache class
        $mustache = new Mustache_Engine(array(
            'loader' => new Mustache_Loader_FilesystemLoader(pathinfo($this->strTemplatePath, PATHINFO_DIRNAME), $options)
        ));

        // Load specified template
        $tpl = $mustache->loadTemplate(pathinfo($this->strTemplatePath, PATHINFO_FILENAME));

        // Process template and return results
        return $tpl->render($arrEdits);
    }

    // Save HTML file, optionally as a PDF
    public function saveFile($strOutputDir, $strProcessedFile="", $boolSaveAsPDF=true): string
    {
        //Check if directory exist
        $this->makeDirectoryCheck($strOutputDir);

        // Convert and save template as PDF if specified, otherwise save as html file
        $strSaveFileName = $strOutputDir . DIRECTORY_SEPARATOR . 'processed_';
        if ($boolSaveAsPDF)
        {
            // Path to file
            $strSaveFileName = $strSaveFileName . pathinfo($this->strTemplatePath, PATHINFO_FILENAME) . '.pdf';

            // Directly convert processed html template as pdf
            Browsershot::html($strProcessedFile)
                // ->paperSize($width, $height)
                // ->landscape()
                ->save($strSaveFileName);  
        } else
        {
            // Path to file
            $strSaveFileName = $strSaveFileName . pathinfo($this->strTemplatePath, PATHINFO_BASENAME);

            // Save processed html template 
            file_put_contents($strSaveFileName, $strProcessedFile);
        }

        return $strSaveFileName;
    }

    // Convert the instantiated (if no other is provided) document to PDF and save at the instantiated path
    public function convertToPDF($strOutputDir, $strInputPath=''): void
    {
        $this->makeDirectoryCheck($strOutputDir);
        $strOutputPath = $strOutputDir . DIRECTORY_SEPARATOR . 'processed_' . pathinfo($this->strTemplatePath, PATHINFO_FILENAME) . '.pdf';
        if (empty($strInputPath)){
            Browsershot::htmlFromFilePath($this->strTemplatePath)->save($strOutputPath);  
        } else {
            Browsershot::htmlFromFilePath($strInputPath)->save($strOutputPath);  
        }
    }
}
?>