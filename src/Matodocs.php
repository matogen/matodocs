<?php
namespace Matogen\Processor;

use Spatie\Browsershot\Browsershot;
use Exception;

/**
 * Parent Matodocs class which extends out to the following classes:
 *  - DocxProcessor: Used for the processing of docx files 
 *  - HTMLProcessor: Used for the processing of html files
 */

abstract class Matodocs{

    /**
     * Absolute path to template path
     * @var string
     */
    public $strTemplatePath;
    
    function __construct($strTemplatePath)
    {
        // Give exception if template path doesn't exist
        try{
            if (!realpath($strTemplatePath)){
                throw new Exception("Provided template path does not exist " . $strTemplatePath);
            } 
            $this->strTemplatePath = $strTemplatePath;
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
            exit();
        }
    }

    // Abstract function for processing and converting files
    abstract public function processAndConvertFile() : string;
    
    // Abstract function for only converting files to PDF
    abstract public function convertToPDF($strOutputDir, $strInputPath=''): void;

    // Abstract function for saving processed files
    abstract public function saveFile($strOutputDir, $strProcessedFile='', $boolSaveAsPDF=true): string;

    // Generate and download image from provided html syntax or url path
    public function generateImage($strInput, $strOutputPath, $intWidth, $intHeight, $strType='html'): void {
        if ($strType === 'html'){
            Browsershot::html($strInput)
            ->windowSize($intWidth, $intHeight)
            ->hideBackground()
            ->save($strOutputPath);
        } else if ($strType === 'url'){
            Browsershot::url($strInput)
            ->windowSize($intWidth, $intHeight)
            ->hideBackground()
            ->save($strOutputPath);
        }
    }

    // Create provided directory if it doesn't exist
    protected function makeDirectoryCheck($strPath): void {
        if (!file_exists($strPath)) {
            mkdir($strPath, 0777, true);
        }
    }
}
?>